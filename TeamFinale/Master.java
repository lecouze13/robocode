package TeamFinale;

/* ---INFO 4A---
BEN AHMED Rayane
GEANO Lorenzo
HOUSSAINI Nabil
MAFROUM Younes
 */

import robocode.*;
import java.awt.Color;
import java.io.*;
import java.awt.geom.Point2D;
import robocode.util.Utils;

public class Master extends TeamRobot {
    double FORWARD = 1; 
    double rand = 185; 
    private boolean messageSent = false;
    private int direction = 1;
    
    public void run() {
        setColors(Color.yellow, Color.black, Color.black);
        setAdjustGunForRobotTurn(true);

        while (true) {
            setTurnRadarRight(10000);
            execute(); 

            if (getEnergy() < 40 && !messageSent) {
                try {
                    broadcastMessage("passer en auto");
                    System.out.println("message envoyé: passer en auto");
                    messageSent = true;
                } catch (IOException e) {}
            }
        }
    }

    public void onScannedRobot(ScannedRobotEvent e) {
        if (isTeammate(e.getName())) return;
        else {
            if (getDistanceRemaining() == 0) {
                FORWARD = -FORWARD;
                setAhead(rand * FORWARD);
            }

            double enemyBearing = this.getHeading() + e.getBearing();

            double tenemyX = getX() + e.getDistance() * Math.sin(Math.toRadians(enemyBearing));
            double tenemyY = getY() + e.getDistance() * Math.cos(Math.toRadians(enemyBearing));

            double bulletPower = Math.min(3.0, getEnergy());
            double myX = getX();
            double myY = getY();
            double absoluteBearing = getHeadingRadians() + e.getBearingRadians();
            double enemyX = getX() + e.getDistance() * Math.sin(absoluteBearing);
            double enemyY = getY() + e.getDistance() * Math.cos(absoluteBearing);
            double enemyHeading = e.getHeadingRadians();
            double enemyVelocity = e.getVelocity();
            double deltaTime = 0;
            double battleFieldHeight = getBattleFieldHeight();
            double battleFieldWidth = getBattleFieldWidth();
            double predictedX = enemyX, predictedY = enemyY;

            while ((++deltaTime) * (20.0 - 3.0 * bulletPower) <
                    Point2D.Double.distance(myX, myY, predictedX, predictedY)) {
                predictedX += Math.sin(enemyHeading) * enemyVelocity;
                predictedY += Math.cos(enemyHeading) * enemyVelocity;

                if (predictedX < 18.0 || predictedY < 18.0 ||
                        predictedX > battleFieldWidth - 18.0 || predictedY > battleFieldHeight - 18.0) {
                    predictedX = Math.min(Math.max(18.0, predictedX), battleFieldWidth - 18.0);
                    predictedY = Math.min(Math.max(18.0, predictedY), battleFieldHeight - 18.0);
                    break;
                }
            }

            double theta = Utils.normalAbsoluteAngle(Math.atan2(predictedX - getX(), predictedY - getY()));
            turnRadarRightRadians(Utils.normalRelativeAngle(absoluteBearing - getRadarHeadingRadians()));

            try {
                broadcastMessage(new Point(predictedX, predictedY));
            } catch (IOException ex) {
                System.out.println("Unable to send order: " + ex);
            }

            double gunTurn = Utils.normalRelativeAngle(theta - getGunHeadingRadians());
            setTurnGunRightRadians(gunTurn);
            fire(3);
        }
    }

    public void onHitByBullet(HitByBulletEvent e) {
        turnLeft(90 - e.getBearing());
    }

    public void onHitWall(HitWallEvent event) {
        setBack(150);
        setTurnRight(90);
    }

    public void onHitRobot(HitRobotEvent event) {
        setBack(50);
    }
	
    public double normalizeBearing(double angle) {
        while (angle > Math.PI) {
            angle -= 2 * Math.PI;
        }
        while (angle < -Math.PI) {
            angle += 2 * Math.PI;
        }
        return angle;
    }
}
