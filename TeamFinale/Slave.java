package TeamFinale;

/* ---INFO 4A---
BEN AHMED Rayane
GEANO Lorenzo
HOUSSAINI Nabil
MAFROUM Younes
 */

import robocode.*;
import robocode.util.Utils;
import robocode.ScannedRobotEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

public class Slave extends TeamRobot {
    boolean peek;
	double moveAmount;
    List<ScannedRobotEvent> enemies = new ArrayList<>();
	double FORWARD = 1;
	double rand = 185;
    boolean modeSansMaitre;
	
	public void run() {
	
		setAdjustGunForRobotTurn(true);
		System.out.println("MyFirstDroid ready.");
	    while(true){
	    if(modeSansMaitre == true) {
	        FORWARD =-FORWARD; 
	        setAhead(rand * FORWARD);
        }
	    scan();
        setTurnRadarRight(10000);
		ahead(100);
		back(100);
		execute();
        }
	}

	public void onMessageReceived(MessageEvent e) {
		
        if (getDistanceRemaining() == 0 ) {
            FORWARD =-FORWARD;
            setAhead(rand * FORWARD);
        }
        setTurnRightRadians(0+Math.PI/2-0.5236*FORWARD*(0>200?1:-1));
	
		if (e.getMessage() instanceof Point) {
			Point p = (Point)e.getMessage();
		
			double dx = p.getX() - this.getX();
			double dy = p.getY() - this.getY();
			
			double theta = Math.toDegrees(Math.atan2(dx,dy));
		
			turnGunRight(normalRelativeAngle(theta - getGunHeading()));
	
			fire(3);
		}
	}


	public void onHitRobot(HitRobotEvent e) {
        if (e.getBearing() > -90 && e.getBearing() < 90) back(100); 
        else ahead(100);
	}
	
    public void onScannedRobot(ScannedRobotEvent e) {
        if (!isTeammate(e.getName())) {
            double enemyBearing = this.getHeading() + e.getBearing();

            // Calcule la position ennemie
            double tenemyX = getX() + e.getDistance() * Math.sin(Math.toRadians(enemyBearing));
            double tenemyY = getY() + e.getDistance() * Math.cos(Math.toRadians(enemyBearing));

            double bulletPower = Math.min(3.0, getEnergy());
            double myX = getX();
            double myY = getY();
            double absoluteBearing = getHeadingRadians() + e.getBearingRadians();
            double enemyX = getX() + e.getDistance() * Math.sin(absoluteBearing);
            double enemyY = getY() + e.getDistance() * Math.cos(absoluteBearing);
            double enemyHeading = e.getHeadingRadians();
            double enemyVelocity = e.getVelocity();
            double deltaTime = 0;
            double battleFieldHeight = getBattleFieldHeight();
            double battleFieldWidth = getBattleFieldWidth();
            double predictedX = enemyX, predictedY = enemyY;

            while ((++deltaTime) * (20.0 - 3.0 * bulletPower) <
                    Point2D.Double.distance(myX, myY, predictedX, predictedY)) {
                predictedX += Math.sin(enemyHeading) * enemyVelocity;
                predictedY += Math.cos(enemyHeading) * enemyVelocity;

                if (predictedX < 18.0 || predictedY < 18.0 ||
                        predictedX > battleFieldWidth - 18.0 || predictedY > battleFieldHeight - 18.0) {
                    predictedX = Math.min(Math.max(18.0, predictedX), battleFieldWidth - 18.0);
                    predictedY = Math.min(Math.max(18.0, predictedY), battleFieldHeight - 18.0);
                    break;
                }
            }

            double theta = Utils.normalAbsoluteAngle(Math.atan2(predictedX - getX(), predictedY - getY()));
            turnRadarRightRadians(Utils.normalRelativeAngle(absoluteBearing - getRadarHeadingRadians()));

            // envoi des positions aux ennemis
            try {
                broadcastMessage(new Point(predictedX, predictedY));
            } catch (IOException ex) {
                System.out.println("Unable to send order: " + ex);
            }

            // Tir aux positions predites
            double gunTurn = Utils.normalRelativeAngle(theta - getGunHeadingRadians());
            setTurnGunRightRadians(gunTurn);
            fire(3);
        }
    }


	@Override
    public void onRobotDeath(RobotDeathEvent event) {
	    String robotName = event.getName();
	    System.out.println(robotName);
        if (robotName.startsWith("Master.Master")) {
            System.out.println("test reusi");
            modeSansMaitre=true;
        }
    }

    private double normalRelativeAngle(double angle) {
        if (angle > -180 && angle <= 180) return angle;
        double fixedAngle = angle;
        while (fixedAngle <= -180) fixedAngle += 360;
        while (fixedAngle > 180) fixedAngle -= 360;
        return fixedAngle;
    }

    public void onHitByBullet(HitByBulletEvent e) {
        turnLeft(90 - e.getBearing());
    }

	public void onHitWall(HitWallEvent event) {
        // S'éloigner du mur
        setBack(150); // Recule de 150 unités
        setTurnRight(90); // Tourne à droite de 90 degrés
    }
}