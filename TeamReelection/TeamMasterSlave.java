package TeamReelection;

/* ---INFO 4A---
BEN AHMED Rayane
GEANO Lorenzo
HOUSSAINI Nabil
MAFROUM Younes
 */

import robocode.*;
import java.awt.Color;
import java.io.*;
import java.awt.geom.Point2D;
import robocode.util.Utils;

public class TeamMasterSlave extends TeamRobot {
	double FORWARD = 1; // choose forward or backward
	double rand = 185; // a random number that changes
	boolean changeLeader = false;
    boolean pretendantLeader = false;
	int direction = 1;
	// ces bool vont définir le rôle du robot.
	boolean master;
	boolean slave;
    private int remainingTeammates; // va compter le nb de membres dans la team
    boolean modeSolo = false; // va permettre de plus envoyer de message si tout le monde est mort

    public void run() {

        remainingTeammates = 5;
        System.out.println("Nombre de membres: "+remainingTeammates);
        // On définit ici si on est master ou slave en début de partie
        if (getEnergy() > 150) {
            master = true;
            setColors(Color.blue, Color.blue, Color.blue);
            // try {
            //     broadcastMessage("Je suis le leader");
            //     System.out.println("Leader défini");
            //     } catch (IOException e) {}
            }
        else {
            slave = true;
            setColors(Color.yellow, Color.yellow, Color.yellow);
        }

        while (true) {
            scan();
            if(master) {
                setTurnRadarRight(10000);
                setTurnLeft(5);
                ahead(30);
                back(30);
    
                execute(); 
                if (getEnergy() < 30 && !changeLeader) {
                    try {
                        broadcastMessage("Changement de leader");
                        System.out.println("Demande de changement de lead envoyé");
                        changeLeader = true;
                        master = false;
                        slave = true;
                    } catch (IOException e) {}
                }
            } else if (slave) {
                ahead(50);
                back(50);
            }
        }   
    }

    public void onMessageReceived(MessageEvent e) {
		
        // if (getDistanceRemaining() == 0 ) {
        //     FORWARD =-FORWARD; setAhead(rand * FORWARD);}
        //     setTurnRightRadians(0+Math.PI/2-0.5236*FORWARD*(0>200?1:-1));
          
        if(slave) {
            //System.out.println("Message reçue");
            if (e.getMessage() instanceof Point) {
            	Point p = (Point)e.getMessage();
				// Calculate x and y to target
				double dx = p.getX() - this.getX();
				double dy = p.getY() - this.getY();
				// Calculate angle to target
				double theta = Math.toDegrees(Math.atan2(dx,dy));
				// Turn gun to target
				turnGunRight(Utils.normalRelativeAngle(theta - getGunHeading()));
				// Fire hard!
				fire(3);
            }
            if (e.getMessage() instanceof String) {
                // partie changement de leader
                if(e.getMessage().equals("Changement de leader")) {
                    try {
                        pretendantLeader = true;
                        broadcastMessage(getEnergy());
                        System.out.println("Energie envoyé");
                    } catch (IOException ex) {
                        System.out.println("Unable to send order: " + ex);
                    }
                }
            }
            // lorsqu'on recoit une energie
            if(e.getMessage() instanceof Double && pretendantLeader && !changeLeader) {
                Double nrjRecue = (Double)e.getMessage(); 
                if(nrjRecue > getEnergy()) {
                    pretendantLeader = false;
                    System.out.println("Je ne suis pas le nouveau leader");
                }
            }
            // Attendre deux secondes
            long startTime = getTime();
            while (getTime() - startTime < 2) {
                execute();
            }
            if(pretendantLeader) {
                slave = false;
                master = true;
                System.out.println("Je suis le nouveau leader");
            }
        }
    }
    
	public void onHitRobot(HitRobotEvent e) {
		// // If he's in front of us, set back up a bit.
		// if (e.getBearing() > -90 && e.getBearing() < 90) {
		// 	back(100);
		// } // else he's in back of us, so set ahead a bit.
		// else {
		// 	ahead(100);
		// }
    }
    
    public void onScannedRobot(ScannedRobotEvent e) {
        if (getDistanceRemaining() == 0) {
            FORWARD = -FORWARD;
            setAhead(rand * FORWARD);
        }
        
        if(master && !modeSolo) {
            // vérifie si c'est pas un allié
            if (isTeammate(e.getName())) {
                return; // Ignorer l'action de tir sur les alliés
            }

            // calcul de la position prédite
            double enemyBearing = this.getHeading() + e.getBearing();

            double tenemyX = getX() + e.getDistance() * Math.sin(Math.toRadians(enemyBearing));
            double tenemyY = getY() + e.getDistance() * Math.cos(Math.toRadians(enemyBearing));

            double bulletPower = Math.min(3.0, getEnergy());
            double myX = getX();
            double myY = getY();
            double absoluteBearing = getHeadingRadians() + e.getBearingRadians();
            double enemyX = getX() + e.getDistance() * Math.sin(absoluteBearing);
            double enemyY = getY() + e.getDistance() * Math.cos(absoluteBearing);
            double enemyHeading = e.getHeadingRadians();
            double enemyVelocity = e.getVelocity();
            double deltaTime = 0;
            double battleFieldHeight = getBattleFieldHeight();
            double battleFieldWidth = getBattleFieldWidth();
            double predictedX = enemyX, predictedY = enemyY;

            while ((++deltaTime) * (20.0 - 3.0 * bulletPower) <
                    Point2D.Double.distance(myX, myY, predictedX, predictedY)) {
                predictedX += Math.sin(enemyHeading) * enemyVelocity;
                predictedY += Math.cos(enemyHeading) * enemyVelocity;

                if (predictedX < 18.0 || predictedY < 18.0 ||
                        predictedX > battleFieldWidth - 18.0 || predictedY > battleFieldHeight - 18.0) {
                    predictedX = Math.min(Math.max(18.0, predictedX), battleFieldWidth - 18.0);
                    predictedY = Math.min(Math.max(18.0, predictedY), battleFieldHeight - 18.0);
                    break;
                }
            }

            double theta = Utils.normalAbsoluteAngle(Math.atan2(predictedX - getX(), predictedY - getY()));
            turnRadarRightRadians(Utils.normalRelativeAngle(absoluteBearing - getRadarHeadingRadians()));

            try {
                broadcastMessage(new Point(predictedX, predictedY));
                //System.out.println("Position envoyé");
                //System.out.println("x: "+predictedX+" y: "+predictedY);
            } catch (IOException ex) {
                System.out.println("Unable to send order: " + ex);
            }

            // tir après avoir envoyé la position
            double gunTurn = normalizeBearing(theta - getGunHeadingRadians());
            setTurnGunRightRadians(gunTurn);
            fire(3);
        }
    }

    public void onHitByBullet(HitByBulletEvent e) {
        //turnLeft(90 - e.getBearing());
    }

    public void onHitWall(HitWallEvent event) {
        setBack(150);
        setTurnRight(90);
    }

    public double normalizeBearing(double angle) {
        while (angle > Math.PI) {
            angle -= 2 * Math.PI;
        }
        while (angle < -Math.PI) {
            angle += 2 * Math.PI;
        }
        return angle;
    }

    public void onRobotDeath(RobotDeathEvent event) {
        String robotName = event.getName();
        if (isTeammate(robotName)) {
            remainingTeammates--;
            if (remainingTeammates == 1) {
                modeSolo = true;
                System.out.println("Mode solo activé");
            }
        }
    }
    
	// 	@Override
    // public void onRobotDeath(RobotDeathEvent event) {
    //     String robotName = event.getName();
    //     System.out.println(robotName);
    //     if (robotName.startsWith("bugger.HiveQueen")) {
    //     System.out.println("test reusi");
    //     }
    // } 
}
